using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using TestProject6.Models;

namespace TestProject6.Data
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Message> Messages { get; set; }
    }
}