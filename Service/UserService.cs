using System.Collections.Generic;
using TestProject6.Models;

namespace TestProject6.Service
{
    public class UserService
    {
        private readonly Dictionary<string, ChatUser> userStorage = new Dictionary<string, ChatUser>();

        public int Count
        {
            get
            {
                return userStorage.Count;
            }
        }

        public ChatUser Get(string username)
        {
            ChatUser user;

            lock (userStorage)
            {
                if(!userStorage.TryGetValue(username, out user)) return null;
            }

            return user;
        }

        public void Add(ChatUser user)
        {
            ChatUser chatUser;

            lock (userStorage)
            {
                if (userStorage.TryGetValue(user.Username, out chatUser))
                {
                    return;
                }

                userStorage.Add(user.Username, user);
            }
        }

        public void Remove(string username)
        {
            ChatUser chatUser;

            lock (userStorage)
            {
                if (!userStorage.TryGetValue(username, out chatUser))
                {
                    return;
                }

                userStorage.Remove(username);
            }
        }
    }
}