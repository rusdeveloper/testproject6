namespace TestProject6.Models
{
    public class ChatUser
    {
        public string ConnectionId { get; set; }
        public string Username { get; set; }
    }
}