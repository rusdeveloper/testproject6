using System;
using System.ComponentModel.DataAnnotations;

namespace TestProject6.Models
{
    public class Message
    {
        public int Id { get; set; }

        [Required]
        public string Text { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:hh\\:mm\\:ss}", ApplyFormatInEditMode = true)]
        public DateTime CreatedDate { get; set; }

        public string ByUser { get; set; }
    }
}