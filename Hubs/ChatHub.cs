using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using TestProject6.Data;
using TestProject6.Models;
using TestProject6.Service;

namespace TestProject6.Hubs
{
    public class ChatHub : Hub
    {
        private readonly UserService userService;
        private readonly DatabaseContext context;

        public ChatHub(UserService userService, DatabaseContext context)
        {
            this.userService = userService;
            this.context = context;
        }

        // private readonly static ConnectionMapping<string> _connections = 
        //     new ConnectionMapping<string>();

        public async Task SendMessage(string messageText)
        {
            ChatUser user = userService.Get(Context.User.Identity.Name);

            if(user == null) await Task.FromException(new Exception("User is undefined"));

            Message message = new Message
            {
                Text = messageText,
                CreatedDate = DateTime.Now,
                ByUser = user.Username
            };

            await context.Messages.AddAsync(message);
            await context.SaveChangesAsync();

            await Clients.All.SendAsync("ReceiveMessage", message);
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            string name = Context.User.Identity.Name;

            userService.Remove(name);
            
            return base.OnDisconnectedAsync(exception);
        }
        public override Task OnConnectedAsync()
        {
            ChatUser user = new ChatUser
            {
                Username = Context.User.Identity.Name,
                ConnectionId = Context.ConnectionId
            };

            userService.Add(user);

            IEnumerable<Message> messages = context.Messages.Where(message => DateTime.Equals(message.CreatedDate.Date, DateTime.Today));

            Clients.Caller.SendAsync("MessagesHistory", messages);

            return base.OnConnectedAsync();
        }

        // public Task SendToReceiver(string message, string targetUsername)
        // {
        //     var chatUserSender = onlineUserService.Get(Context.User.Identity.Name);
        //     var chatUserReciever = onlineUserService.Get(targetUsername);
            
        //     return Clients.Client(chatUserReciever.ConnectionId).SendAsync("SendToReceiver", message, chatUserSender);
        // }

        // public Task SendToAll(string message)
        // {
        //     var chatUserSender = onlineUserService.Get(Context.User.Identity.Name);

        //     return Clients.All.SendAsync("SendToAll", message, chatUserSender);
        // }
    }
}