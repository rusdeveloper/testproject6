﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TestProject6.Data;
using TestProject6.Hubs;
using TestProject6.Service;

namespace TestProject6
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<UserService>();

            // Database
            services.AddDbContext<DatabaseContext>(options => options.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));

            // Authentication & authorization
            services.AddSession();

            services.AddAuthorization(options => 
            {
                options.AddPolicy("UserPolicy", policy => 
                {
                    policy.RequireAuthenticatedUser();
                });
            });
            
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options => {
                    options.LoginPath = "/auth/login";
                    options.LogoutPath = "/auth/logout";
                    options.AccessDeniedPath = "/auth/login";
                    options.Cookie.HttpOnly = true;
                    // options.Cookie.Expiration = TimeSpan.FromMinutes(5);
                });
            
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // SignalR
            // services.AddCors(options => 
            // {
            //     options.AddPolicy("CorsPolicy", policy => 
            //     {
            //         policy.AllowAnyHeader()
            //                 .AllowAnyMethod()
            //                 .AllowCredentials()
            //                 .WithOrigins("http://localhost:4200");
            //     });
            // });
            services.AddSignalR();            

            // MVC
            services.AddRouting(options => options.LowercaseUrls = true);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Errors / Exceptions
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            // Others
            // app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            // Authentication
            app.UseSession();
            
            app.UseAuthentication();

            // SignalR
            // app.UseCors("CorsPolicy");
            app.UseSignalR(routes => 
            {
                routes.MapHub<ChatHub>("/chat");
            });

            // MVC
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
