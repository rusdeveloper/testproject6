using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TestProject6.Data;
using TestProject6.Models;
using TestProject6.Service;
using TestProject6.ViewModel;

namespace TestProject6.Controllers
{
    public class AuthController : Controller
    {
        private readonly UserService userService;

        public AuthController(UserService userService)
        {
            this.userService = userService;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            var onlineUser = userService.Get(loginViewModel.Username);

            if(onlineUser != null)
            {
                ModelState.AddModelError("exists_user", "This user already exists");

                return BadRequest(ModelState);
            }

            var user = new ChatUser
            {
                ConnectionId = HttpContext.Connection.Id,
                Username = loginViewModel.Username
            };

            userService.Add(user);

            var claims = new List<Claim>
			{
				new Claim(ClaimTypes.Name, loginViewModel.Username)
			};
 
			var identity = new ClaimsIdentity(claims, "login");
 
			ClaimsPrincipal principal = new ClaimsPrincipal(identity);
            
            await HttpContext.SignInAsync(principal);

            return Redirect("/");
        }

        [Authorize]
        public async Task<IActionResult> Logout()
        {
            userService.Remove(HttpContext.User.Identity.Name);

            await HttpContext.SignOutAsync();

            return Redirect("/");
        }
    }
}