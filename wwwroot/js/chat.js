// The following sample code uses modern ECMAScript 6 features 
// that aren't supported in Internet Explorer 11.
// To convert the sample for environments that do not support ECMAScript 6, 
// such as Internet Explorer 11, use a transpiler such as 
// Babel at http://babeljs.io/. 
//
// See Es5-chat.js for a Babel transpiled version of the following code:

function addMessage(message) {
    document.getElementById("messageText").value = '';

    var options = {hour: 'numeric', minute: 'numeric', second: 'numeric'};
    var messageDate = new Date(message.createdDate).toLocaleTimeString("ru-RU", options);

    var liHtml = "<li><strong>" + messageDate + ", " + message.byUser + "</strong>: <span>" + message.text + "</span></li>";
    
    document.getElementById("messagesList").insertAdjacentHTML('beforeend', liHtml);
}

const connection = new signalR.HubConnectionBuilder().withUrl("/chat").configureLogging(signalR.LogLevel.Information).build();

connection.on("ReceiveMessage", (message) => {
    addMessage(message)
});

connection.on("MessagesHistory", (messages) => {
    messages.forEach(function(message){
        addMessage(message);
    });
});

document.getElementById("sendButton").addEventListener("click", event => {
    const message = document.getElementById("messageText").value;    
    
    connection.invoke("SendMessage", message).catch(err => console.error(err.toString()));

    event.preventDefault();
});

connection.start().catch(err => console.error(err.toString()));